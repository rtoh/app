package com.devbase.mavejavafxapp.controller;

import javafx.beans.property.*;

public class ArtistBand {
    private StringProperty firstname;
    private StringProperty lastname;
    private StringProperty country;
    private IntegerProperty bandartiststartyear;
    private IntegerProperty bandartistendyear;
    private StringProperty bandname;
    private StringProperty alias;
    private IntegerProperty formationyear;
    private IntegerProperty finalyear;
    private StringProperty genre;

    //Constructor
    public ArtistBand(String firstname, String lastname, String country, Integer bandartiststartyear, Integer bandartistendyear,
                      String bandname, String alias, Integer formationyear, Integer finalyear, String genre) {
        this.firstname = new SimpleStringProperty(firstname);
        this.lastname = new SimpleStringProperty(lastname);
        this.country = new SimpleStringProperty(country);
        this.bandartiststartyear = new SimpleIntegerProperty(bandartiststartyear);
        this.bandartistendyear = new SimpleIntegerProperty(bandartistendyear);
        this.bandname = new SimpleStringProperty(bandname);
        this.alias = new SimpleStringProperty(alias);
        this.formationyear = new SimpleIntegerProperty(formationyear);
        this.finalyear = new SimpleIntegerProperty(finalyear);
        this.genre = new SimpleStringProperty(genre);
    }

    public int getfinalyear() {
        return finalyear.get();
    }

    public void setfinalyear(int finalyear) {
        this.finalyear.set(finalyear);
    }

    public IntegerProperty finalyearProperty() {
        return finalyear;
    }

    public int getformationyear() {
        return formationyear.get();
    }

    public void setformationyear(int formationyear) {
        this.formationyear.set(formationyear);
    }

    public IntegerProperty formationyearProperty() {
        return formationyear;
    }

    public int getbandartistendyear() {
        return bandartistendyear.get();
    }

    public void setbandartistendyear(int bandartistendyear) {
        this.bandartistendyear.set(bandartistendyear);
    }

    public IntegerProperty bandartistendyearProperty() {
        return bandartistendyear;
    }

    public int getbandartiststartyear() {
        return bandartiststartyear.get();
    }

    public void setbandartiststartyear(int bandartiststartyear) {
        this.bandartiststartyear.set(bandartiststartyear);
    }

    public IntegerProperty bandartiststartyearProperty() {
        return bandartiststartyear;
    }

    public String getalias() {
        return alias.get();
    }

    public void setalias(String alias) {
        this.alias.set(alias);
    }

    public StringProperty aliasProperty() {
        return alias;
    }

    public String getfirstname() {
        return firstname.get();
    }

    public void setfirstname(String firstname) {
        this.firstname.set(firstname);
    }

    public StringProperty firstnameProperty() {
        return firstname;
    }

    public String getlastname() {
        return lastname.get();
    }

    public void setlastname(String lastname) {
        this.lastname.set(lastname);
    }

    public StringProperty lastnameProperty() {
        return lastname;
    }

    public String getcountry() {
        return country.get();
    }

    public void setcountry(String country) {
        this.country.set(country);
    }

    public StringProperty countryProperty() {
        return country;
    }

    public String getbandname() {
        return bandname.get();
    }

    public void setbandname(String bandname) {
        this.bandname.set(bandname);
    }

    public StringProperty bandnameProperty() {
        return bandname;
    }

    public String getgenre() {
        return genre.get();
    }

    public void setgenre(String genre) {
        this.genre.set(genre);
    }

    public StringProperty genreProperty() {
        return genre;
    }


}

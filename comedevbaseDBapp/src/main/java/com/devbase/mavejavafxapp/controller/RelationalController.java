package com.devbase.mavejavafxapp.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class RelationalController {

    @FXML
    private Button exit;

    @FXML
    private Button backtutorial;

    @FXML
    public void back(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBtutorial.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("Tutorial");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize() throws Exception {
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });

        backtutorial.setOnAction((event) -> {
            try {
                back(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
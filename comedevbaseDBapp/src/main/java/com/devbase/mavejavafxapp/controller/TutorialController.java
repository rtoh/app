package com.devbase.mavejavafxapp.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

public class TutorialController {
    @FXML
    private Button exit;

    @FXML
    private Button mainmenu;

    @FXML
    private Hyperlink erdiagram;

    @FXML
    private Hyperlink relationalschema;

    @FXML
    public void back(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBappJavaFX.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("App");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void image1(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBrelational.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("Relational");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void image2(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBer.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("ER");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize() throws Exception {
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });

        mainmenu.setOnAction((event) -> {
            try {
                back(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        erdiagram.setOnAction((event) -> {
            try {
                image2(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        relationalschema.setOnAction((event) -> {
            try {
                image1(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
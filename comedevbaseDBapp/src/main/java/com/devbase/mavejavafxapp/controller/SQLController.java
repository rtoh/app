package com.devbase.mavejavafxapp.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.sql.*;

public class SQLController {

    //необходимо для подключения к бд
    private static final String URL = "jdbc:mysql://localhost:3306/testserver?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    @FXML
    private Button exit;

    @FXML
    private Button mainmenu;

    @FXML
    private Button uploadData;

    @FXML
    private TextField statusline;

    @FXML
    private TextArea scriptarea;

    @FXML
    public void back(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBappJavaFX.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("App");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
            e.printStackTrace();
        }
    }

    @FXML
    //метод читающий скл скрипт из поля и организует очередьь запросов на выполнение(если их несколько)
    //отправляет эти запросы возможно надо перенести в MainApp
    //это полностью работает для нескольких скриптов подряд.
    // Запрос должен заканчиваться ';'
    public void scriptDo(ActionEvent event) {
        StringBuilder strScript = new StringBuilder(scriptarea.getText());
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
            e.printStackTrace();
        }
        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement statement = connection.createStatement();
            String[] line = strScript.toString().split("[;]");
            int f = line.length;

            for (int i = 0; i < f; i++) {
                if (i + 1 != f)
                    line[i] = line[i].replaceAll("\\\n", "") + ";";
                else
                    line[i] = line[i].replaceAll("\\\n", "");

                System.out.println(line[i]);
                statement.addBatch(line[i]);
            }

            statement.executeBatch();
            statement.clearBatch();
            statement.close();
            connection.close();
            statusline.clear();
            statusline.appendText("Script done");
            scriptarea.clear();
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }
    }

    @FXML
    private void initialize() throws Exception {
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });
        mainmenu.setOnAction((event) -> {
            try {
                back(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        uploadData.setOnAction((event -> {
            try {
                scriptDo(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        }));
    }
}
package com.devbase.mavejavafxapp.controller;

import javafx.beans.property.*;

public class AlbumTrack {
    private StringProperty albumname;
    private StringProperty groupname;
    private StringProperty labelname;
    private IntegerProperty albumreleaseyear;
    private IntegerProperty albumlength;
    private StringProperty genre;
    private StringProperty trackname;
    private IntegerProperty tracklength;
    private IntegerProperty trackreleaseyear;
    private StringProperty language;

    //Constructor
    public AlbumTrack(String albumname, String labelname, Integer albumreleaseyear,
                      Integer albumlength, String genre, String trackname, Integer tracklength, Integer trackreleaseyear, String language, String groupname) {
        this.albumreleaseyear = new SimpleIntegerProperty(albumreleaseyear);
        this.albumname = new SimpleStringProperty(albumname);
        this.labelname = new SimpleStringProperty(labelname);
        this.genre = new SimpleStringProperty(genre);
        this.albumlength = new SimpleIntegerProperty(albumlength);
        this.trackname = new SimpleStringProperty(trackname);
        this.language = new SimpleStringProperty(language);
        this.tracklength = new SimpleIntegerProperty(tracklength);
        this.trackreleaseyear = new SimpleIntegerProperty(trackreleaseyear);
        this.groupname = new SimpleStringProperty(groupname);
    }

    public int getAlbumreleaseyear() {
        return albumreleaseyear.get();
    }

    public void setAlbumreleaseyear(int albumreleaseyear) {
        this.albumreleaseyear.set(albumreleaseyear);
    }

    public IntegerProperty albumreleaseyearProperty() {
        return albumreleaseyear;
    }

    public String getalbumname() {
        return albumname.get();
    }

    public void setalbumname(String albumname) {
        this.albumname.set(albumname);
    }

    public StringProperty albumnameProperty() {
        return albumname;
    }

    public String getlabelname() {
        return labelname.get();
    }

    public void setlabelname(String labelname) {
        this.labelname.set(labelname);
    }

    public StringProperty labelnameProperty() {
        return labelname;
    }

    public String gettrackname() {
        return trackname.get();
    }

    public void settrackname(String trackname) {
        this.trackname.set(trackname);
    }

    public StringProperty tracknameProperty() {
        return trackname;
    }

    public int getalbumlength() {
        return albumlength.get();
    }

    public void setalbumlength(int albumlength) {
        this.albumlength.set(albumlength);
    }

    public IntegerProperty albumlengthProperty() {
        return albumlength;
    }

    public String getgenre() {
        return genre.get();
    }

    public void setgenre(String genre) {
        this.genre.set(genre);
    }

    public StringProperty genreProperty() {
        return genre;
    }

    public String getlanguage() {
        return language.get();
    }

    public void setlanguage(String language) {
        this.language.set(language);
    }

    public StringProperty languageProperty() {
        return language;
    }

    public int gettracklength() {
        return tracklength.get();
    }

    public void settracklength(int tracklength) {
        this.tracklength.set(tracklength);
    }

    public IntegerProperty tracklengthProperty() {
        return tracklength;
    }

    public int gettrackreleaseyear() {
        return trackreleaseyear.get();
    }

    public void settrackreleaseyear(int trackreleaseyear) {
        this.trackreleaseyear.set(trackreleaseyear);
    }

    public IntegerProperty trackreleaseyearProperty() {
        return trackreleaseyear;
    }

    public String getgroupname() {
        return groupname.get();
    }

    public void setgroupname(String groupname) {
        this.groupname.set(groupname);
    }

    public StringProperty groupnameProperty() {
        return groupname;
    }
}

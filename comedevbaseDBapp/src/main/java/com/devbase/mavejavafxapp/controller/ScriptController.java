package com.devbase.mavejavafxapp.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScriptController {
    private static final String URL = "jdbc:mysql://localhost:3306/testserver?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    @FXML
    private TextArea artistfirstname, artistlastname, alias, countryname, bandartiststartyearmin, bandartiststartyearmax;
    @FXML
    private TextArea bandartistendyearmin, bandartistendyearmax, bandname, genrename, bandformationyearmin, bandformationyearmax;
    @FXML
    private TextArea bandfinalyearmin, bandfinalyearmax;

    @FXML
    private TextArea tracklengthmin, trackreleaseyearmax, trackreleaseyearmin, lablename, albumname, albumreleaseyearmax, group_name;
    @FXML
    private TextArea albumlengthmax, albumreleaseyearmin, albumlengthmin, genre, tracklengthmax, languagename, trackname;

    @FXML
    private TextField statusline;
    @FXML
    private Button exit;
    @FXML
    private Button mainmenu;
    @FXML
    private Button outAPP, outAPP1;
    @FXML
    private Button outXML, outXML1;
    @FXML
    private Button clear, clear1;

    @FXML
    private TableView<AlbumTrack> table;
    @FXML
    private TableColumn<AlbumTrack, Integer> columnalbum_release_year;
    @FXML
    private TableColumn<AlbumTrack, String> columnalbum_name;
    @FXML
    private TableColumn<AlbumTrack, String> columngroup_name;
    @FXML
    private TableColumn<AlbumTrack, String> columnlabel_name;
    @FXML
    private TableColumn<AlbumTrack, String> columngenre2;
    @FXML
    private TableColumn<AlbumTrack, Integer> columnalbum_length;
    @FXML
    private TableColumn<AlbumTrack, String> columntrack_name;
    @FXML
    private TableColumn<AlbumTrack, String> columnlanguage;
    @FXML
    private TableColumn<AlbumTrack, Integer> columntrack_length;
    @FXML
    private TableColumn<AlbumTrack, Integer> columntrack_release_year;

    @FXML
    private TableView<ArtistBand> table2;
    @FXML
    private TableColumn<ArtistBand, String> columnfirst_name;
    @FXML
    private TableColumn<ArtistBand, String> columnlast_name;
    @FXML
    private TableColumn<ArtistBand, String> columncountry;
    @FXML
    private TableColumn<ArtistBand, Integer> columnbandartist_start_year;
    @FXML
    private TableColumn<ArtistBand, Integer> columnbandartist_end_year;
    @FXML
    private TableColumn<ArtistBand, String> columnband_name;
    @FXML
    private TableColumn<ArtistBand, Integer> columnalias;
    @FXML
    private TableColumn<ArtistBand, Integer> columnformation_year;
    @FXML
    private TableColumn<ArtistBand, Integer> columnfinal_year;
    @FXML
    private TableColumn<ArtistBand, String> columngenre;

    private ObservableList<AlbumTrack> data;
    private ObservableList<ArtistBand> data2;

    @FXML
    private void outIterface1(ActionEvent event) {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            data = FXCollections.observableArrayList();

            PreparedStatement statement = connection.prepareStatement("SELECT album.name, band.name, label.name," +
                    "                     album.release_year, album.length, GROUP_CONCAT(distinct genre.name SEPARATOR ', ') AS genres," +
                    "                     track.name, track.length, track.release_year, language.name FROM album_track, band," +
                    "                     album_genre, track_genre, genre, (track LEFT JOIN language ON track.language_id = language.language_id)," +
                    "                     (album LEFT JOIN label ON album.label_id = label.label_id) WHERE album.album_id = album_track.album_id and" +
                    "                     track.track_id = album_track.track_id and album.album_id = album_genre.album_id and" +
                    "                     album_genre.genre_id = genre.genre_id and track.track_id = track_genre.track_id and" +
                    "                     track_genre.genre_id = genre.genre_id and ((album.label_id = label.label_id) or (album.label_id IS NULL  ))" +
                    "                     and (track.language_id = language.language_id or (track.language_id IS NULL)) and album.name LIKE ? and" +
                    "                     (label.name LIKE ? or (label.name IS NULL and ? IS NULL)) and album.band_id = band.band_id" +
                    "                     and ((album.release_year >= ? and album.release_year <= ?) or (? IS NULL and album.release_year <= ?)" +
                    "                     or (album.release_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((album.length >= ? and album.length <= ?)" +
                    "                     or (? IS NULL and album.length <= ?) or (album.length >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and" +
                    "                     genre.name LIKE ? and track.name LIKE ? and ((track.length >= ? and track.length <= ?) or (? IS NULL and track.length <= ?)" +
                    "                     or (track.length >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((track.release_year >= ? and track.release_year <= ?)" +
                    "                     or (? IS NULL and track.release_year <= ?) or (track.release_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "                     and (language.name LIKE ? or (language.name IS NULL and ? IS NULL)) and band.name LIKE ? GROUP BY album.name, label.name," +
                    "                     album.release_year, album.length, track.name, track.length, track.release_year, language.name");

            statement.setString(1, '%' + albumname.getText() + '%');
            if (lablename.getText().equals("")) {
                statement.setString(2, "%%");
                statement.setNull(3, Types.VARCHAR);
            } else {
                statement.setString(2, '%' + lablename.getText() + '%');
                statement.setString(3, '%' + lablename.getText() + '%');
            }
            if (albumreleaseyearmin.getText().equals("")) {
                statement.setNull(4, Types.OTHER);
                statement.setNull(6, Types.OTHER);
                statement.setNull(8, Types.OTHER);
                statement.setNull(10, Types.OTHER);
            } else {
                statement.setString(4, albumreleaseyearmin.getText());
                statement.setString(6, albumreleaseyearmin.getText());
                statement.setString(8, albumreleaseyearmin.getText());
                statement.setString(10, albumreleaseyearmin.getText());
            }
            if (albumreleaseyearmax.getText().equals("")) {
                statement.setNull(5, Types.OTHER);
                statement.setNull(7, Types.OTHER);
                statement.setNull(9, Types.OTHER);
                statement.setNull(11, Types.OTHER);
            } else {
                statement.setString(5, albumreleaseyearmax.getText());
                statement.setString(7, albumreleaseyearmax.getText());
                statement.setString(9, albumreleaseyearmax.getText());
                statement.setString(11, albumreleaseyearmax.getText());
            }
            if (albumlengthmin.getText().equals("")) {
                statement.setNull(12, Types.OTHER);
                statement.setNull(14, Types.OTHER);
                statement.setNull(16, Types.OTHER);
                statement.setNull(18, Types.OTHER);
            } else {
                statement.setString(12, albumlengthmin.getText());
                statement.setString(14, albumlengthmin.getText());
                statement.setString(16, albumlengthmin.getText());
                statement.setString(18, albumlengthmin.getText());
            }
            if (albumlengthmax.getText().equals("")) {
                statement.setNull(13, Types.OTHER);
                statement.setNull(15, Types.OTHER);
                statement.setNull(17, Types.OTHER);
                statement.setNull(19, Types.OTHER);
            } else {
                statement.setString(13, albumlengthmax.getText());
                statement.setString(15, albumlengthmax.getText());
                statement.setString(17, albumlengthmax.getText());
                statement.setString(19, albumlengthmax.getText());
            }
            statement.setString(20, '%' + genre.getText() + '%');
            statement.setString(21, '%' + trackname.getText() + '%');
            if (tracklengthmin.getText().equals("")) {
                statement.setNull(22, Types.OTHER);
                statement.setNull(24, Types.OTHER);
                statement.setNull(26, Types.OTHER);
                statement.setNull(28, Types.OTHER);
            } else {
                statement.setString(22, tracklengthmin.getText());
                statement.setString(24, tracklengthmin.getText());
                statement.setString(26, tracklengthmin.getText());
                statement.setString(28, tracklengthmin.getText());
            }
            if (tracklengthmax.getText().equals("")) {
                statement.setNull(23, Types.OTHER);
                statement.setNull(25, Types.OTHER);
                statement.setNull(27, Types.OTHER);
                statement.setNull(29, Types.OTHER);
            } else {
                statement.setString(23, tracklengthmax.getText());
                statement.setString(25, tracklengthmax.getText());
                statement.setString(27, tracklengthmax.getText());
                statement.setString(29, tracklengthmax.getText());
            }
            if (trackreleaseyearmin.getText().equals("")) {
                statement.setNull(30, Types.OTHER);
                statement.setNull(32, Types.OTHER);
                statement.setNull(34, Types.OTHER);
                statement.setNull(36, Types.OTHER);
            } else {
                statement.setString(30, trackreleaseyearmin.getText());
                statement.setString(32, trackreleaseyearmin.getText());
                statement.setString(34, trackreleaseyearmin.getText());
                statement.setString(36, trackreleaseyearmin.getText());
            }
            if (trackreleaseyearmax.getText().equals("")) {
                statement.setNull(31, Types.OTHER);
                statement.setNull(33, Types.OTHER);
                statement.setNull(35, Types.OTHER);
                statement.setNull(37, Types.OTHER);
            } else {
                statement.setString(31, trackreleaseyearmax.getText());
                statement.setString(33, trackreleaseyearmax.getText());
                statement.setString(35, trackreleaseyearmax.getText());
                statement.setString(37, trackreleaseyearmax.getText());
            }
            if (languagename.getText().equals("")) {
                statement.setString(38, "%%");
                statement.setNull(39, Types.VARCHAR);
            } else {
                statement.setString(38, '%' + languagename.getText() + '%');
                statement.setString(39, '%' + languagename.getText() + '%');
            }
            statement.setString(40, '%' + group_name.getText() + '%');

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                data.add(new AlbumTrack(resultSet.getString(1), resultSet.getString(3), resultSet.getInt(4),
                        resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7), resultSet.getInt(8),
                        resultSet.getInt(9), resultSet.getString(10), resultSet.getString(2)));
            }
            resultSet.close();
            statement.close();
            connection.close();
            statusline.appendText("ssd");
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }
        columnalbum_name.setCellValueFactory(new PropertyValueFactory<>("albumname"));
        columngroup_name.setCellValueFactory(new PropertyValueFactory<>("groupname"));
        columnlabel_name.setCellValueFactory(new PropertyValueFactory<>("labelname"));
        columnalbum_release_year.setCellValueFactory(new PropertyValueFactory<>("albumreleaseyear"));
        columnalbum_length.setCellValueFactory(new PropertyValueFactory<>("albumlength"));
        columngenre2.setCellValueFactory(new PropertyValueFactory<>("genre"));
        columntrack_name.setCellValueFactory(new PropertyValueFactory<>("trackname"));
        columntrack_length.setCellValueFactory(new PropertyValueFactory<>("tracklength"));
        columntrack_release_year.setCellValueFactory(new PropertyValueFactory<>("trackreleaseyear"));
        columnlanguage.setCellValueFactory(new PropertyValueFactory<>("language"));

        table.setItems(null);
        table.setItems(data);
        if(!statusline.getText().equals("")) {
            statusline.clear();
            statusline.appendText("Done");
        }
    }

    @FXML
    private void outIterface(ActionEvent event) {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            data2 = FXCollections.observableArrayList();

            PreparedStatement statement = connection.prepareStatement("SELECT artist.first_name, artist.last_name," +
                    "  country.name, band_artist.start_year, band_artist.end_year, band.name, artist.alias, band.formation_year," +
                    "  band.final_year, GROUP_CONCAT(distinct genre.name SEPARATOR ',') AS genres FROM" +
                    "  band_artist, band, genre, band_genre, (artist LEFT JOIN country ON artist.country_id= country.country_id)" +
                    " WHERE artist.artist_id = band_artist.artist_id and band.band_id = band_artist.band_id and" +
                    " band.band_id = band_genre.band_id and genre.genre_id = band_genre.genre_id and (artist.country_id = country.country_id" +
                    " or (artist.country_id IS NULL)) and artist.first_name LIKE ? and artist.last_name LIKE ? and " +
                    "(artist.alias LIKE ? or (artist.alias IS NULL and ? IS NULL)) and band.name LIKE ? and " +
                    "((band.formation_year >= ? and band.formation_year <= ?) or (? IS NULL and band.formation_year <= ?)" +
                    "or (band.formation_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((band.final_year >= ? and band.final_year <= ?) or" +
                    "(? IS NULL and band.final_year <= ?) or (band.final_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    " and genre.name LIKE ? and (country.name LIKE ? or (country.name IS NULL and ? IS NULL)) and" +
                    "((band_artist.start_year >= ? and band_artist.start_year <= ?) or" +
                    "   (? IS NULL and band_artist.start_year <= ?) or (band_artist.start_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "    AND ((band_artist.end_year >= ? and band_artist.end_year <= ?) or" +
                    "     (? IS NULL and band_artist.end_year <= ?) or (band_artist.end_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "GROUP BY artist.first_name, artist.last_name," +
                    "  country.name, band_artist.start_year, band_artist.end_year, band.name, artist.alias, band.formation_year, band.final_year");

            statement.setString(1, '%' + artistfirstname.getText() + '%');
            statement.setString(2, '%' + artistlastname.getText() + '%');
            if (alias.getText().equals("")) {
                statement.setString(3, "%%");
                statement.setNull(4, Types.VARCHAR);
            } else {
                statement.setString(3, '%' + alias.getText() + '%');
                statement.setString(4, '%' + alias.getText() + '%');
            }
            statement.setString(5, '%' + bandname.getText() + '%');
            if (bandformationyearmin.getText().equals("")) {
                statement.setNull(6, Types.OTHER);
                statement.setNull(8, Types.OTHER);
                statement.setNull(10, Types.OTHER);
                statement.setNull(12, Types.OTHER);
            } else {
                statement.setString(6, bandformationyearmin.getText());
                statement.setString(8, bandformationyearmin.getText());
                statement.setString(10, bandformationyearmin.getText());
                statement.setString(12, bandformationyearmin.getText());
            }
            if (bandformationyearmax.getText().equals("")) {
                statement.setNull(7, Types.OTHER);
                statement.setNull(9, Types.OTHER);
                statement.setNull(11, Types.OTHER);
                statement.setNull(13, Types.OTHER);
            } else {
                statement.setString(7, bandformationyearmax.getText());
                statement.setString(9, bandformationyearmax.getText());
                statement.setString(11, bandformationyearmax.getText());
                statement.setString(13, bandformationyearmax.getText());
            }
            if (bandfinalyearmin.getText().equals("")) {
                statement.setNull(14, Types.OTHER);
                statement.setNull(16, Types.OTHER);
                statement.setNull(18, Types.OTHER);
                statement.setNull(20, Types.OTHER);
            } else {
                statement.setString(14, bandfinalyearmin.getText());
                statement.setString(16, bandfinalyearmin.getText());
                statement.setString(18, bandfinalyearmin.getText());
                statement.setString(20, bandfinalyearmin.getText());
            }
            if (bandfinalyearmax.getText().equals("")) {
                statement.setNull(15, Types.OTHER);
                statement.setNull(17, Types.OTHER);
                statement.setNull(19, Types.OTHER);
                statement.setNull(21, Types.OTHER);
            } else {
                statement.setString(15, bandfinalyearmax.getText());
                statement.setString(17, bandfinalyearmax.getText());
                statement.setString(19, bandfinalyearmax.getText());
                statement.setString(21, bandfinalyearmax.getText());
            }
            statement.setString(22, '%' + genrename.getText() + '%');
            if (alias.getText().equals("")) {
                statement.setString(23, "%%");
                statement.setNull(24, Types.VARCHAR);
            } else {
                statement.setString(23, '%' + alias.getText() + '%');
                statement.setString(24, '%' + alias.getText() + '%');
            }
            if (bandartiststartyearmin.getText().equals("")) {
                statement.setNull(31, Types.OTHER);
                statement.setNull(25, Types.OTHER);
                statement.setNull(27, Types.OTHER);
                statement.setNull(29, Types.OTHER);
            } else {
                statement.setString(31, bandartiststartyearmin.getText());
                statement.setString(25, bandartiststartyearmin.getText());
                statement.setString(27, bandartiststartyearmin.getText());
                statement.setString(29, bandartiststartyearmin.getText());
            }
            if (bandartiststartyearmax.getText().equals("")) {
                statement.setNull(32, Types.OTHER);
                statement.setNull(26, Types.OTHER);
                statement.setNull(28, Types.OTHER);
                statement.setNull(30, Types.OTHER);
            } else {
                statement.setString(32, bandartiststartyearmax.getText());
                statement.setString(26, bandartiststartyearmax.getText());
                statement.setString(28, bandartiststartyearmax.getText());
                statement.setString(30, bandartiststartyearmax.getText());
            }
            if (bandartistendyearmin.getText().equals("")) {
                statement.setNull(33, Types.OTHER);
                statement.setNull(35, Types.OTHER);
                statement.setNull(37, Types.OTHER);
                statement.setNull(39, Types.OTHER);
            } else {
                statement.setString(33, bandartistendyearmin.getText());
                statement.setString(35, bandartistendyearmin.getText());
                statement.setString(37, bandartistendyearmin.getText());
                statement.setString(39, bandartistendyearmin.getText());
            }
            if (bandartistendyearmax.getText().equals("")) {
                statement.setNull(34, Types.OTHER);
                statement.setNull(36, Types.OTHER);
                statement.setNull(38, Types.OTHER);
                statement.setNull(40, Types.OTHER);
            } else {
                statement.setString(34, bandartistendyearmax.getText());
                statement.setString(36, bandartistendyearmax.getText());
                statement.setString(38, bandartistendyearmax.getText());
                statement.setString(40, bandartistendyearmax.getText());
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                data2.add(new ArtistBand(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getInt(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7),
                        resultSet.getInt(8), resultSet.getInt(9), resultSet.getString(10)));
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }

        columnfirst_name.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        columnlast_name.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        columncountry.setCellValueFactory(new PropertyValueFactory<>("country"));
        columnbandartist_start_year.setCellValueFactory(new PropertyValueFactory<>("bandartiststartyear"));
        columnbandartist_end_year.setCellValueFactory(new PropertyValueFactory<>("bandartistendyear"));
        columnband_name.setCellValueFactory(new PropertyValueFactory<>("bandname"));
        columnalias.setCellValueFactory(new PropertyValueFactory<>("alias"));
        columnformation_year.setCellValueFactory(new PropertyValueFactory<>("formationyear"));
        columnfinal_year.setCellValueFactory(new PropertyValueFactory<>("finalyear"));
        columngenre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        table2.setItems(null);
        table2.setItems(data2);
        statusline.clear();
        statusline.appendText("Done");
    }

    @FXML
    private void outToXML1(ActionEvent event) {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            data = FXCollections.observableArrayList();

            PreparedStatement statement = connection.prepareStatement("SELECT album.name, band.name, label.name," +
                    "                     album.release_year, album.length, GROUP_CONCAT(distinct genre.name SEPARATOR ', ') AS genres," +
                    "                     track.name, track.text, track.length, track.release_year, language.name FROM album_track, band," +
                    "                     album_genre, track_genre, genre, (track LEFT JOIN language ON track.language_id = language.language_id)," +
                    "                     (album LEFT JOIN label ON album.label_id = label.label_id) WHERE album.album_id = album_track.album_id and" +
                    "                     track.track_id = album_track.track_id and album.album_id = album_genre.album_id and" +
                    "                     album_genre.genre_id = genre.genre_id and track.track_id = track_genre.track_id and" +
                    "                     track_genre.genre_id = genre.genre_id and ((album.label_id = label.label_id) or (album.label_id IS NULL  ))" +
                    "                     and (track.language_id = language.language_id or (track.language_id IS NULL)) and album.name LIKE ? and" +
                    "                     (label.name LIKE ? or (label.name IS NULL and ? IS NULL)) and album.band_id = band.band_id" +
                    "                     and ((album.release_year >= ? and album.release_year <= ?) or (? IS NULL and album.release_year <= ?)" +
                    "                     or (album.release_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((album.length >= ? and album.length <= ?)" +
                    "                     or (? IS NULL and album.length <= ?) or (album.length >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and" +
                    "                     genre.name LIKE ? and track.name LIKE ? and ((track.length >= ? and track.length <= ?) or (? IS NULL and track.length <= ?)" +
                    "                     or (track.length >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((track.release_year >= ? and track.release_year <= ?)" +
                    "                     or (? IS NULL and track.release_year <= ?) or (track.release_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "                     and (language.name LIKE ? or (language.name IS NULL and ? IS NULL)) and band.name LIKE ? GROUP BY album.name, label.name," +
                    "                     album.release_year, album.length, track.name, track.length, track.release_year, language.name");

            statement.setString(1, '%' + albumname.getText() + '%');
            if (lablename.getText().equals("")) {
                statement.setString(2, "%%");
                statement.setNull(3, Types.VARCHAR);
            } else {
                statement.setString(2, '%' + lablename.getText() + '%');
                statement.setString(3, '%' + lablename.getText() + '%');
            }
            if (albumreleaseyearmin.getText().equals("")) {
                statement.setNull(4, Types.OTHER);
                statement.setNull(6, Types.OTHER);
                statement.setNull(8, Types.OTHER);
                statement.setNull(10, Types.OTHER);
            } else {
                statement.setString(4, albumreleaseyearmin.getText());
                statement.setString(6, albumreleaseyearmin.getText());
                statement.setString(8, albumreleaseyearmin.getText());
                statement.setString(10, albumreleaseyearmin.getText());
            }
            if (albumreleaseyearmax.getText().equals("")) {
                statement.setNull(5, Types.OTHER);
                statement.setNull(7, Types.OTHER);
                statement.setNull(9, Types.OTHER);
                statement.setNull(11, Types.OTHER);
            } else {
                statement.setString(5, albumreleaseyearmax.getText());
                statement.setString(7, albumreleaseyearmax.getText());
                statement.setString(9, albumreleaseyearmax.getText());
                statement.setString(11, albumreleaseyearmax.getText());
            }
            if (albumlengthmin.getText().equals("")) {
                statement.setNull(12, Types.OTHER);
                statement.setNull(14, Types.OTHER);
                statement.setNull(16, Types.OTHER);
                statement.setNull(18, Types.OTHER);
            } else {
                statement.setString(12, albumlengthmin.getText());
                statement.setString(14, albumlengthmin.getText());
                statement.setString(16, albumlengthmin.getText());
                statement.setString(18, albumlengthmin.getText());
            }
            if (albumlengthmax.getText().equals("")) {
                statement.setNull(13, Types.OTHER);
                statement.setNull(15, Types.OTHER);
                statement.setNull(17, Types.OTHER);
                statement.setNull(19, Types.OTHER);
            } else {
                statement.setString(13, albumlengthmax.getText());
                statement.setString(15, albumlengthmax.getText());
                statement.setString(17, albumlengthmax.getText());
                statement.setString(19, albumlengthmax.getText());
            }
            statement.setString(20, '%' + genre.getText() + '%');
            statement.setString(21, '%' + trackname.getText() + '%');
            if (tracklengthmin.getText().equals("")) {
                statement.setNull(22, Types.OTHER);
                statement.setNull(24, Types.OTHER);
                statement.setNull(26, Types.OTHER);
                statement.setNull(28, Types.OTHER);
            } else {
                statement.setString(22, tracklengthmin.getText());
                statement.setString(24, tracklengthmin.getText());
                statement.setString(26, tracklengthmin.getText());
                statement.setString(28, tracklengthmin.getText());
            }
            if (tracklengthmax.getText().equals("")) {
                statement.setNull(23, Types.OTHER);
                statement.setNull(25, Types.OTHER);
                statement.setNull(27, Types.OTHER);
                statement.setNull(29, Types.OTHER);
            } else {
                statement.setString(23, tracklengthmax.getText());
                statement.setString(25, tracklengthmax.getText());
                statement.setString(27, tracklengthmax.getText());
                statement.setString(29, tracklengthmax.getText());
            }
            if (trackreleaseyearmin.getText().equals("")) {
                statement.setNull(30, Types.OTHER);
                statement.setNull(32, Types.OTHER);
                statement.setNull(34, Types.OTHER);
                statement.setNull(36, Types.OTHER);
            } else {
                statement.setString(30, trackreleaseyearmin.getText());
                statement.setString(32, trackreleaseyearmin.getText());
                statement.setString(34, trackreleaseyearmin.getText());
                statement.setString(36, trackreleaseyearmin.getText());
            }
            if (trackreleaseyearmax.getText().equals("")) {
                statement.setNull(31, Types.OTHER);
                statement.setNull(33, Types.OTHER);
                statement.setNull(35, Types.OTHER);
                statement.setNull(37, Types.OTHER);
            } else {
                statement.setString(31, trackreleaseyearmax.getText());
                statement.setString(33, trackreleaseyearmax.getText());
                statement.setString(35, trackreleaseyearmax.getText());
                statement.setString(37, trackreleaseyearmax.getText());
            }
            if (languagename.getText().equals("")) {
                statement.setString(38, "%%");
                statement.setNull(39, Types.VARCHAR);
            } else {
                statement.setString(38, '%' + languagename.getText() + '%');
                statement.setString(39, '%' + languagename.getText() + '%');
            }
            statement.setString(40, '%' + group_name.getText() + '%');


            ResultSet resultSet = statement.executeQuery();

            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.newDocument();
                Element results = doc.createElement("Results");
                doc.appendChild(results);
                ResultSetMetaData rsmd = resultSet.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (resultSet.next()) {
                    Element row = doc.createElement("Row");
                    results.appendChild(row);

                    for (int i = 1; i <= colCount; i++) {
                        String columnName = rsmd.getColumnName(i);
                        Object value = resultSet.getObject(i);
                        Element node = doc.createElement(columnName);

                        if (value != null) {
                            node.appendChild(doc.createTextNode(value.toString()));
                            row.appendChild(node);
                        } else {
                            node.appendChild(doc.createTextNode(""));
                            row.appendChild(node);
                        }
                    }
                }

                DOMSource domSource = new DOMSource(doc);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                java.io.StringWriter sw = new java.io.StringWriter();
                StreamResult sr = new StreamResult(sw);
                transformer.transform(domSource, sr);
                String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n" + sr.getWriter().toString();
                List<String> stringlist = Arrays.asList(xmlString);

                try {
                    Path file = Paths.get("output.xml");
                    Files.write(file, stringlist, Charset.forName("UTF-16"));
                } catch (Exception e) {
                    statusline.clear();
                    statusline.appendText(e.getMessage());
                }

                //System.out.println(xmlString);

                resultSet.close();
                statement.close();
                connection.close();
                statusline.clear();
                statusline.appendText("Done");
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }
    }

    @FXML
    private void outToXML(ActionEvent event) {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            data2 = FXCollections.observableArrayList();

            PreparedStatement statement = connection.prepareStatement("SELECT artist.first_name, artist.last_name," +
                    "  country.name, band_artist.start_year, band_artist.end_year, band.name, artist.alias, band.formation_year," +
                    "  band.final_year, GROUP_CONCAT(distinct genre.name SEPARATOR ',') AS genres FROM" +
                    "  band_artist, band, genre, band_genre, (artist LEFT JOIN country ON artist.country_id= country.country_id)" +
                    " WHERE artist.artist_id = band_artist.artist_id and band.band_id = band_artist.band_id and" +
                    " band.band_id = band_genre.band_id and genre.genre_id = band_genre.genre_id and (artist.country_id = country.country_id" +
                    " or (artist.country_id IS NULL)) and artist.first_name LIKE ? and artist.last_name LIKE ? and " +
                    "(artist.alias LIKE ? or (artist.alias IS NULL and ? IS NULL)) and band.name LIKE ? and " +
                    "((band.formation_year >= ? and band.formation_year <= ?) or (? IS NULL and band.formation_year <= ?)" +
                    "or (band.formation_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL)) and ((band.final_year >= ? and band.final_year <= ?) or" +
                    "(? IS NULL and band.final_year <= ?) or (band.final_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    " and genre.name LIKE ? and (country.name LIKE ? or (country.name IS NULL and ? IS NULL)) and" +
                    "((band_artist.start_year >= ? and band_artist.start_year <= ?) or" +
                    "   (? IS NULL and band_artist.start_year <= ?) or (band_artist.start_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "    AND ((band_artist.end_year >= ? and band_artist.end_year <= ?) or" +
                    "     (? IS NULL and band_artist.end_year <= ?) or (band_artist.end_year >= ? and ? IS NULL) or (? IS NULL and ? IS NULL))" +
                    "GROUP BY artist.first_name, artist.last_name," +
                    "  country.name, band_artist.start_year, band_artist.end_year, band.name, artist.alias, band.formation_year, band.final_year");

            statement.setString(1, '%' + artistfirstname.getText() + '%');
            statement.setString(2, '%' + artistlastname.getText() + '%');
            if (alias.getText().equals("")) {
                statement.setString(3, "%%");
                statement.setNull(4, Types.VARCHAR);
            } else {
                statement.setString(3, '%' + alias.getText() + '%');
                statement.setString(4, '%' + alias.getText() + '%');
            }
            statement.setString(5, '%' + bandname.getText() + '%');
            if (bandformationyearmin.getText().equals("")) {
                statement.setNull(6, Types.OTHER);
                statement.setNull(8, Types.OTHER);
                statement.setNull(10, Types.OTHER);
                statement.setNull(12, Types.OTHER);
            } else {
                statement.setString(6, bandformationyearmin.getText());
                statement.setString(8, bandformationyearmin.getText());
                statement.setString(10, bandformationyearmin.getText());
                statement.setString(12, bandformationyearmin.getText());
            }
            if (bandformationyearmax.getText().equals("")) {
                statement.setNull(7, Types.OTHER);
                statement.setNull(9, Types.OTHER);
                statement.setNull(11, Types.OTHER);
                statement.setNull(13, Types.OTHER);
            } else {
                statement.setString(7, bandformationyearmax.getText());
                statement.setString(9, bandformationyearmax.getText());
                statement.setString(11, bandformationyearmax.getText());
                statement.setString(13, bandformationyearmax.getText());
            }
            if (bandfinalyearmin.getText().equals("")) {
                statement.setNull(14, Types.OTHER);
                statement.setNull(16, Types.OTHER);
                statement.setNull(18, Types.OTHER);
                statement.setNull(20, Types.OTHER);
            } else {
                statement.setString(14, bandfinalyearmin.getText());
                statement.setString(16, bandfinalyearmin.getText());
                statement.setString(18, bandfinalyearmin.getText());
                statement.setString(20, bandfinalyearmin.getText());
            }
            if (bandfinalyearmax.getText().equals("")) {
                statement.setNull(15, Types.OTHER);
                statement.setNull(17, Types.OTHER);
                statement.setNull(19, Types.OTHER);
                statement.setNull(21, Types.OTHER);
            } else {
                statement.setString(15, bandfinalyearmax.getText());
                statement.setString(17, bandfinalyearmax.getText());
                statement.setString(19, bandfinalyearmax.getText());
                statement.setString(21, bandfinalyearmax.getText());
            }
            statement.setString(22, '%' + genrename.getText() + '%');
            if (alias.getText().equals("")) {
                statement.setString(23, "%%");
                statement.setNull(24, Types.VARCHAR);
            } else {
                statement.setString(23, '%' + alias.getText() + '%');
                statement.setString(24, '%' + alias.getText() + '%');
            }
            if (bandartiststartyearmin.getText().equals("")) {
                statement.setNull(31, Types.OTHER);
                statement.setNull(25, Types.OTHER);
                statement.setNull(27, Types.OTHER);
                statement.setNull(29, Types.OTHER);
            } else {
                statement.setString(31, bandartiststartyearmin.getText());
                statement.setString(25, bandartiststartyearmin.getText());
                statement.setString(27, bandartiststartyearmin.getText());
                statement.setString(29, bandartiststartyearmin.getText());
            }
            if (bandartiststartyearmax.getText().equals("")) {
                statement.setNull(32, Types.OTHER);
                statement.setNull(26, Types.OTHER);
                statement.setNull(28, Types.OTHER);
                statement.setNull(30, Types.OTHER);
            } else {
                statement.setString(32, bandartiststartyearmax.getText());
                statement.setString(26, bandartiststartyearmax.getText());
                statement.setString(28, bandartiststartyearmax.getText());
                statement.setString(30, bandartiststartyearmax.getText());
            }
            if (bandartistendyearmin.getText().equals("")) {
                statement.setNull(33, Types.OTHER);
                statement.setNull(35, Types.OTHER);
                statement.setNull(37, Types.OTHER);
                statement.setNull(39, Types.OTHER);
            } else {
                statement.setString(33, bandartistendyearmin.getText());
                statement.setString(35, bandartistendyearmin.getText());
                statement.setString(37, bandartistendyearmin.getText());
                statement.setString(39, bandartistendyearmin.getText());
            }
            if (bandartistendyearmax.getText().equals("")) {
                statement.setNull(34, Types.OTHER);
                statement.setNull(36, Types.OTHER);
                statement.setNull(38, Types.OTHER);
                statement.setNull(40, Types.OTHER);
            } else {
                statement.setString(34, bandartistendyearmax.getText());
                statement.setString(36, bandartistendyearmax.getText());
                statement.setString(38, bandartistendyearmax.getText());
                statement.setString(40, bandartistendyearmax.getText());
            }

            ResultSet resultSet = statement.executeQuery();

            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.newDocument();
                Element results = doc.createElement("Results");
                doc.appendChild(results);
                ResultSetMetaData rsmd = resultSet.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (resultSet.next()) {
                    Element row = doc.createElement("Row");
                    results.appendChild(row);

                    for (int i = 1; i <= colCount; i++) {
                        String columnName = rsmd.getColumnName(i);
                        Object value = resultSet.getObject(i);
                        Element node = doc.createElement(columnName);

                        if (value != null) {
                            node.appendChild(doc.createTextNode(value.toString()));
                            row.appendChild(node);
                        } else {
                            node.appendChild(doc.createTextNode(""));
                            row.appendChild(node);
                        }
                    }
                }

                DOMSource domSource = new DOMSource(doc);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                java.io.StringWriter sw = new java.io.StringWriter();
                StreamResult sr = new StreamResult(sw);
                transformer.transform(domSource, sr);
                String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n" + sr.getWriter().toString();

                List<String> stringlist = Arrays.asList(xmlString);

                try {
                    Path file = Paths.get("output.xml");
                    Files.write(file, stringlist, Charset.forName("UTF-16"));
                } catch (Exception e) {
                    statusline.clear();
                    statusline.appendText(e.getMessage());
                }

                //System.out.println(xmlString);

                resultSet.close();
                statement.close();
                connection.close();
                statusline.clear();
                statusline.appendText("Done");
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }
    }

    @FXML
    public void back(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBappJavaFX.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("App");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
        }
    }

    @FXML
    public void clearText(ActionEvent event) {
        artistfirstname.clear();
        artistlastname.clear();
        alias.clear();
        countryname.clear();
        bandartiststartyearmin.clear();
        bandartiststartyearmax.clear();
        bandartistendyearmin.clear();
        bandartistendyearmax.clear();
        bandname.clear();
        genrename.clear();
        bandformationyearmin.clear();
        bandformationyearmax.clear();
        bandfinalyearmin.clear();
        bandfinalyearmax.clear();
    }

    @FXML
    public void clearText1(ActionEvent event) {
        tracklengthmin.clear();
        trackreleaseyearmax.clear();
        trackreleaseyearmin.clear();
        lablename.clear();
        albumname.clear();
        albumreleaseyearmax.clear();
        group_name.clear();
        albumlengthmax.clear();
        albumreleaseyearmin.clear();
        albumlengthmin.clear();
        genre.clear();
        tracklengthmax.clear();
        languagename.clear();
        trackname.clear();
    }

    @FXML
    private void initialize() throws Exception {
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });
        mainmenu.setOnAction((event) -> {
            try {
                back(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        outAPP.setOnAction((event) -> {
            try {
                outIterface1(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        outXML.setOnAction((event) -> {
            try {
                outToXML1(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        outAPP1.setOnAction((event) -> {
            try {
                outIterface(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        outXML1.setOnAction((event) -> {
            try {
                outToXML(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        clear.setOnAction((event) -> {
            try {
                clearText1(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        clear1.setOnAction((event) -> {
            try {
                clearText(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
    }
}
package com.devbase.mavejavafxapp.controller;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController<T extends Button, Lable, TextArea> {
    @FXML
    private Button exit;

    @FXML
    private Button script;

    @FXML
    private Button tutorial;

    @FXML
    private Button addData;

    @FXML
    private Button SQLData;

    @FXML
    public void scripter(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBscript.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("Script");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void adder(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBadd.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("Add");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void SQLer(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBSQL.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("SQL code");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void tutorialer(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBtutorial.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("Tutorial");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize() throws Exception {
        script.setOnAction((event) -> {
            try {
                scripter(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        tutorial.setOnAction((event) -> {
            try {
                tutorialer(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        addData.setOnAction((event) -> {
            try {
                adder(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        SQLData.setOnAction((event) -> {
            try {
                SQLer(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });
    }
}
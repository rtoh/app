package com.devbase.mavejavafxapp.controller;

import com.mysql.cj.core.exceptions.NumberOutOfRange;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.ExecutionException;
import java.util.function.UnaryOperator;

import static jdk.nashorn.internal.runtime.JSType.isNumber;

public class AddController {

    //необходимо для подключения к бд
    private static final String URL = "jdbc:mysql://localhost:3306/testserver?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    @FXML
    private Button exit, mainmenu, addData, clear;

    @FXML
    private DatePicker artistDateDeath, artistDateBirth;

    @FXML
    private TextArea statusline, bandName, bandYear, bandFinYear, albumtrackTrackNumber, artistLastName, artistAlias;
    @FXML
    private TextArea bandartistEndYear, artistFirstName, languageName, countryName, bandartistStartYear;
    @FXML
    private TextArea albumName, albumLength, albumReleaseYear, trackLength;
    @FXML
    private TextArea trackName, trackText, trackReleaseYear, lableName, genreName;

    private String pathToXML;
    private String nameOfXML;

    @FXML
    //Возврат в главное меню
    public void back(ActionEvent mouseEvent) {
        try {
            Parent chartPageParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/DBappJavaFX.fxml"));
            Scene chartPageScene = new Scene(chartPageParent);
            Stage appStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            appStage.hide();
            appStage.setTitle("App");
            appStage.setScene(chartPageScene);
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void clearbt(ActionEvent event) {
        artistDateDeath.getEditor().clear();
        artistDateBirth.getEditor().clear();
        bandName.clear();
        bandYear.clear();
        bandFinYear.clear();
        albumtrackTrackNumber.clear();
        artistLastName.clear();
        artistAlias.clear();
        bandartistEndYear.clear();
        artistFirstName.clear();
        languageName.clear();
        countryName.clear();
        bandartistStartYear.clear();
        albumName.clear();
        albumLength.clear();
        albumReleaseYear.clear();
        trackLength.clear();
        trackName.clear();
        trackText.clear();
        trackReleaseYear.clear();
        lableName.clear();
        genreName.clear();
    }

    @FXML
    public void addInterf(ActionEvent event) throws SQLException {
        String bandNamestr = bandName.getText().toLowerCase();
        String bandYearstr = bandYear.getText();
        String bandFinYearstr = bandFinYear.getText();
        String albumtrackTrackNumberstr = albumtrackTrackNumber.getText();
        String artistLastNamestr = artistLastName.getText().toLowerCase();
        String artistAliasstr = artistAlias.getText().toLowerCase();
        String bandartistStartYearstr = bandartistStartYear.getText();
        String bandartistEndYearstr = bandartistEndYear.getText();
        String artistFirstNamestr = artistFirstName.getText();
        String languageNamestr = languageName.getText().toLowerCase();
        String countryNamestr = countryName.getText().toLowerCase();
        String albumNamestr = albumName.getText().toLowerCase();
        String albumLengthstr = albumLength.getText();
        String albumReleaseYearstr = albumReleaseYear.getText();
        String trackLengthstr = trackLength.getText();
        String trackNamestr = trackName.getText().toLowerCase();
        String trackTextstr = trackText.getText().toLowerCase();
        String trackReleaseYearstr = trackReleaseYear.getText();
        String lableNamestr = lableName.getText().toLowerCase();
        String genreNamestr = genreName.getText().toLowerCase();
        statusline.clear();
        //чекаем инты
        bandYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!(bandYearstr.equals(""))) {
            if (!bandYearstr.matches("-?[1-9][0-9]*")) {
                bandYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.appendText("Ошибка в поле с датой образования группы. \n");
                return;
            }
        }
        bandFinYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!(bandFinYearstr.equals(""))) {
            if (!bandFinYearstr.matches("-?[1-9][0-9]*")) {
                bandFinYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.appendText("Ошибка в поле с датой развала группы. \n");
                return;
            }
        }
        albumReleaseYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!albumReleaseYearstr.matches("-?[1-9][0-9]*")) {
            albumReleaseYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с датой альбома. \n");
            return;
        }
        bandartistEndYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!(bandartistEndYearstr.equals(""))) {
            if (!bandartistEndYearstr.matches("-?[1-9][0-9]*")) {
                bandartistEndYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.appendText("Ошибка в поле с датой окончания присутствия артиста в группе. \n");
                return;
            }
        }
        bandartistStartYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!bandartistStartYearstr.matches("-?[1-9][0-9]*")) {
            bandartistStartYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с датой появления артиста в группы. \n");
            return;
        }
        trackReleaseYear.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!trackReleaseYearstr.matches("-?[1-9][0-9]*")) {
            trackReleaseYear.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с датой релиза трека. \n");
            return;
        }
        albumtrackTrackNumber.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!albumtrackTrackNumberstr.matches("-?[1-9][0-9]*")) {
            albumtrackTrackNumber.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с номером трека. \n");
            return;
        }
        albumLength.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!albumLengthstr.matches("-?[1-9][0-9]*")) {
            albumLength.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с количеством песен в альбоме. \n");
            return;
        }
        trackLength.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (!trackLengthstr.matches("-?[1-9][0-9]*")) {
            trackLength.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с длинной трека. \n");
            return;
        }
        albumName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
        if (albumNamestr.equals("")) {
            albumName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
            statusline.appendText("Ошибка в поле с длинной трека. \n");
            return;
        }
        //в правильной очередности чекать таблицы и добавлять

        Driver driver = new com.mysql.cj.jdbc.Driver();
        DriverManager.registerDriver(driver);
        Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
        Connection connection1 = DriverManager.getConnection(URL, USER, PASSWORD);
        PreparedStatement album1 = null;
        PreparedStatement trackgenre2 = null;
        PreparedStatement genrest = null;
        PreparedStatement labelst = null;
        PreparedStatement countryst = null;
        PreparedStatement languagest = null;
        PreparedStatement trackst = null;
        PreparedStatement artistst = null;
        PreparedStatement bandst = null;
        PreparedStatement bandartist1 = null;
        PreparedStatement track1 = null;
        PreparedStatement trackgenre = null;
        PreparedStatement bandartist = null;
        PreparedStatement bandgenre = null;
        PreparedStatement albumst = null;
        PreparedStatement albumgenre = null;
        PreparedStatement albumTrack = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SET FOREIGN_KEY_CHECKS=0;");
            statement.executeUpdate();
            statement.close();
            connection.setAutoCommit(false);
            //проверяем обязательные поля
            genreName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (genreNamestr.equals("")) {
                genreName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.clear();
                statusline.appendText("Ошибка в поле жанр. \n");
                return;
            }
            trackName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (trackNamestr.equals("")) {
                trackName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.clear();
                statusline.appendText("Ошибка в поле название трека. \n");
                return;
            }
            artistFirstName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (artistFirstNamestr.equals("")) {
                artistFirstName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.clear();
                statusline.appendText("Ошибка в поле имени актера. \n");
                return;
            }
            artistLastName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (artistLastNamestr.equals("")) {
                artistLastName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.clear();
                statusline.appendText("Ошибка в поле фамилии актера. \n");
                return;
            }
            bandName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (bandNamestr.equals("")) {
                bandName.setStyle("-fx-background-color: red; -fx-text-fill: black;");
                statusline.clear();
                statusline.appendText("Ошибка в поле названия группы. \n");
                return;
            }

            //добавляем genre
            PreparedStatement genrecheck = connection1.prepareStatement(
                    "SELECT genre_id FROM genre" + " WHERE LOWER(name)=?;");
            genrecheck.setString(1, genreNamestr);
            ResultSet resultSet = genrecheck.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                genrest = connection.prepareStatement("REPLACE INTO genre (name) VALUES (?);");
                genrest.setString(1, genreNamestr);
                genrest.executeUpdate();
            }
            genrecheck.close();
            //добавляем label
            lableName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (!lableNamestr.equals("")) {
                PreparedStatement labelcheck = connection1.prepareStatement(
                        "SELECT label_id FROM label" + " WHERE LOWER(name)=?;");
                labelcheck.setString(1, lableNamestr);
                resultSet = labelcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    labelst = connection.prepareStatement("REPLACE INTO label (name) VALUES (?);");
                    labelst.setString(1, lableNamestr);
                    labelst.executeUpdate();
                }
                labelcheck.close();
            }
            //добавляем country
            countryName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (!countryNamestr.equals("")) {
                PreparedStatement countrycheck = connection1.prepareStatement(
                        "SELECT country_id FROM country" + " WHERE LOWER(name)=?;");
                countrycheck.setString(1, countryNamestr);
                resultSet = countrycheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    countryst = connection.prepareStatement("REPLACE INTO country (name) VALUES (?);");
                    countryst.setString(1, countryNamestr);
                    countryst.executeUpdate();
                }
                countrycheck.close();
            }
            //добавляем language

            languageName.setStyle("-fx-background-color: def; -fx-text-fill: black;");
            if (!languageNamestr.equals("")) {
                PreparedStatement langcheck = connection1.prepareStatement(
                        "SELECT language_id FROM language" + " WHERE LOWER (name)=?;");
                langcheck.setString(1, languageNamestr);
                resultSet = langcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    languagest = connection.prepareStatement("REPLACE INTO language (name) VALUES (?);");
                    languagest.setString(1, languageNamestr);
                    languagest.executeUpdate();
                }
                langcheck.close();
            }
            //добавляем track
            String languageid = "";
            if (!languageNamestr.equals("")) {
                PreparedStatement trackcheck = connection1.prepareStatement(
                        "SELECT language_id FROM language WHERE LOWER (name)=?;");
                trackcheck.setString(1, languageNamestr);
                resultSet = trackcheck.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    resultSet.next();
                    languageid = resultSet.getString("language_id");
                } else {
                    PreparedStatement trackcheck12 = connection1.prepareStatement("SELECT " +
                            "max(language_id)+1 FROM language;");
                    resultSet = trackcheck12.executeQuery();
                    resultSet.next();
                    languageid = resultSet.getString("max(language_id)+1");

                }
                trackst = connection1.prepareStatement(
                        "SELECT name, length, language_id, release_year " +
                                "FROM track" + " WHERE LOWER (name)=? AND length=? AND " +
                                "language_id=? AND release_year=?;");
                trackst.setString(1, trackNamestr);
                trackst.setString(2, trackLengthstr);
                trackst.setString(3, languageid);
                trackst.setString(4, trackReleaseYearstr);
                ResultSet resultSet1 = trackst.executeQuery();
                if (!resultSet1.isBeforeFirst()) {
                    track1 = connection.prepareStatement("REPLACE INTO track (name, length, " +
                            "language_id, text, release_year) VALUES (?,?,?,?,?);");
                    track1.setString(1, trackNamestr);
                    track1.setString(2, trackLengthstr);
                    track1.setString(3, languageid);
                    if (trackTextstr.equals("")) {
                        track1.setNull(4, Types.OTHER);
                    } else {
                        track1.setString(4, trackTextstr);
                    }
                    track1.setString(5, trackReleaseYearstr);
                    track1.executeUpdate();
                }
                trackcheck.close();
            } else {
                PreparedStatement trackcheck = connection1.prepareStatement(
                        "SELECT track_id " +
                                "FROM track WHERE LOWER (name)=? AND length=? AND " +
                                "language_id IS NULL AND release_year=?;");
                trackcheck.setString(1, trackNamestr);
                trackcheck.setString(2, trackLengthstr);
                trackcheck.setString(3, trackReleaseYearstr);
                resultSet = trackcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    trackst = connection.prepareStatement("REPLACE INTO track (name, length, " +
                            "language_id, text, release_year) VALUES (?,?,?,?,?);");
                    trackst.setString(1, trackNamestr);
                    trackst.setString(2, trackLengthstr);
                    trackst.setNull(3, Types.TINYINT);
                    if (trackTextstr.equals("")) {
                        trackst.setNull(4, Types.OTHER);
                    } else
                        trackst.setString(4, trackTextstr);
                    trackst.setString(5, trackReleaseYearstr);
                    trackst.executeUpdate();
                }
                trackcheck.close();
            }
            //добавляем artist
            String countid = "";
            if (countryNamestr.equals("")) {
                PreparedStatement artistkcheck = connection1.prepareStatement(
                        "SELECT artist_id FROM artist" +
                                " WHERE LOWER (first_name)=? AND LOWER (last_name)=? " +
                                "AND (LOWER (alias)=? OR(alias IS NULL AND ? IS NULL)) " +
                                "AND country_id IS NULL " +
                                "AND (date_of_birth=? OR(date_of_birth IS NULL AND ? IS NULL)) " +
                                "AND (date_of_death=? OR(date_of_death IS NULL AND ? IS NULL));");
                artistkcheck.setString(1, artistFirstNamestr);
                artistkcheck.setString(2, artistLastNamestr);
                if (!artistAliasstr.equals("")) {
                    artistkcheck.setString(3, artistAliasstr);
                    artistkcheck.setString(4, artistAliasstr);
                } else {
                    artistkcheck.setNull(3, Types.VARCHAR);
                    artistkcheck.setNull(4, Types.VARCHAR);
                }
                LocalDate date = artistDateDeath.getValue();
                if (date != null) {
                    Date date2 = Date.valueOf(date);
                    artistkcheck.setDate(6, date2);
                    artistkcheck.setDate(5, date2);
                } else {
                    artistkcheck.setNull(6, Types.DATE);
                    artistkcheck.setNull(5, Types.DATE);
                }
                date = artistDateBirth.getValue();
                if (date != null) {
                    Date date2 = Date.valueOf(date);
                    artistkcheck.setDate(8, date2);
                    artistkcheck.setDate(7, date2);
                } else {
                    artistkcheck.setNull(8, Types.DATE);
                    artistkcheck.setNull(7, Types.DATE);
                }
                resultSet = artistkcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    artistst = connection.prepareStatement(
                            "REPLACE INTO artist (first_name, last_name," +
                                    " alias, country_id, date_of_birth, date_of_death) " +
                                    "VALUES (?, ?, ?, ?, ?, ?);");
                    artistst.setString(1, artistFirstNamestr);
                    artistst.setString(2, artistLastNamestr);
                    if (!artistAliasstr.equals("")) {
                        artistst.setString(3, artistAliasstr);
                    } else {
                        artistst.setNull(3, Types.VARCHAR);
                    }
                    artistst.setNull(4, Types.TINYINT);
                    date = artistDateDeath.getValue();
                    if (date != null) {
                        Date date2 = Date.valueOf(date);
                        artistst.setDate(5, date2);
                    } else {
                        artistst.setNull(5, Types.DATE);
                    }
                    date = artistDateBirth.getValue();
                    if (date != null) {
                        Date date2 = Date.valueOf(date);
                        artistst.setDate(6, date2);
                    } else {
                        artistst.setNull(6, Types.DATE);
                    }
                    artistst.executeUpdate();
                }
                artistkcheck.close();
            } else {
                PreparedStatement artistkcheck = connection1.prepareStatement("SELECT country_id " +
                        "FROM country WHERE LOWER(name)=?;");
                artistkcheck.setString(1, countryNamestr);
                resultSet = artistkcheck.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    resultSet.next();
                    countid = resultSet.getString("country_id");
                } else {
                    PreparedStatement artistcheck4 = connection1.prepareStatement("SELECT max(country_id)+1 " +
                            " FROM country;");
                    resultSet = artistcheck4.executeQuery();
                    resultSet.next();
                    countid = resultSet.getString("max(country_id)+1");
                    artistcheck4.close();
                }
                PreparedStatement artistkcheck1 = connection1.prepareStatement(
                        "SELECT artist_id FROM artist" +
                                " WHERE LOWER (first_name)=? AND LOWER (last_name)=? " +
                                "AND (LOWER (alias)=? OR(alias IS NULL AND ? IS NULL)) AND country_id=? " +
                                "AND (date_of_birth=? OR(date_of_birth IS NULL AND ? IS NULL)) " +
                                "AND( date_of_death=? OR(date_of_death IS NULL AND ? IS NULL));");
                artistkcheck1.setString(1, artistFirstNamestr);
                artistkcheck1.setString(2, artistLastNamestr);
                if (!artistAliasstr.equals("")) {
                    artistkcheck1.setString(3, artistAliasstr);
                    artistkcheck1.setString(4, artistAliasstr);
                } else {
                    artistkcheck1.setNull(3, Types.VARCHAR);
                    artistkcheck1.setNull(4, Types.VARCHAR);
                }
                artistkcheck1.setString(5, countid);
                LocalDate date = artistDateDeath.getValue();
                if (date != null) {
                    Date date2 = Date.valueOf(date);
                    artistkcheck1.setDate(6, date2);
                    artistkcheck1.setDate(7, date2);
                } else {
                    artistkcheck1.setNull(6, Types.DATE);
                    artistkcheck1.setNull(7, Types.DATE);
                }
                date = artistDateBirth.getValue();
                if (date != null) {
                    Date date2 = Date.valueOf(date);
                    artistkcheck1.setDate(8, date2);
                    artistkcheck1.setDate(9, date2);
                } else {
                    artistkcheck1.setNull(8, Types.DATE);
                    artistkcheck1.setNull(9, Types.DATE);
                }
                resultSet = artistkcheck1.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    artistst = connection.prepareStatement(
                            "REPLACE INTO artist (first_name, last_name," +
                                    " alias, country_id, date_of_birth, date_of_death) " +
                                    "VALUES (?, ?, ?, ?, ?, ?);");
                    artistst.setString(1, artistFirstNamestr);
                    artistst.setString(2, artistLastNamestr);
                    if (!artistAliasstr.equals("")) {
                        artistst.setString(3, artistAliasstr);
                    } else {
                        artistst.setNull(3, Types.VARCHAR);
                    }
                    artistst.setString(4, countid);
                    date = artistDateDeath.getValue();
                    if (date != null) {
                        Date date2 = Date.valueOf(date);
                        artistst.setDate(5, date2);
                    } else {
                        artistst.setNull(5, Types.DATE);
                    }
                    date = artistDateBirth.getValue();
                    if (date != null) {
                        Date date2 = Date.valueOf(date);
                        artistst.setDate(6, date2);
                    } else {
                        artistst.setNull(6, Types.DATE);
                    }
                    artistst.executeUpdate();
                }
                artistkcheck.close();
                artistkcheck1.close();
            }
            //band
            if (countryNamestr.equals("")) {
                PreparedStatement bandcheck = connection1.prepareStatement(
                        "SELECT band_id FROM band" +
                                " WHERE LOWER (name)=? AND country_id IS NULL " +
                                "AND (formation_year=? OR(formation_year IS NULL AND ? IS NULL)) " +
                                "AND (final_year=? OR(final_year IS NULL AND ? IS NULL));");
                bandcheck.setString(1, bandNamestr);
                if (!bandYearstr.equals("")) {
                    bandcheck.setString(2, bandYearstr);
                    bandcheck.setString(3, bandYearstr);
                } else {
                    bandcheck.setNull(2, Types.OTHER);
                    bandcheck.setNull(3, Types.OTHER);
                }
                if (!bandFinYearstr.equals("")) {
                    bandcheck.setString(4, bandFinYearstr);
                    bandcheck.setString(5, bandFinYearstr);
                } else {
                    bandcheck.setNull(4, Types.OTHER);
                    bandcheck.setNull(5, Types.OTHER);
                }
                resultSet = bandcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    bandst = connection.prepareStatement("REPLACE INTO band (name, country_id," +
                            " formation_year, final_year) VALUES (?, ?, ?, ?);");
                    bandst.setString(1, bandNamestr);
                    bandst.setNull(2, Types.TINYINT);
                    if (!bandYearstr.equals("")) {
                        bandst.setString(3, bandYearstr);
                    } else {
                        bandst.setNull(3, Types.OTHER);
                    }
                    if (!bandFinYearstr.equals("")) {
                        bandst.setString(4, bandFinYearstr);
                    } else {
                        bandst.setNull(4, Types.OTHER);
                    }
                    bandst.executeUpdate();
                }
                bandcheck.close();
            } else {
                PreparedStatement bandcheck = connection1.prepareStatement(
                        "SELECT band_id FROM band" +
                                " WHERE LOWER (name)=? AND country_id=? " +
                                "AND (formation_year=? OR(formation_year IS NULL AND ? IS NULL)) " +
                                "AND (final_year=? OR(final_year IS NULL AND ? IS NULL));");
                bandcheck.setString(1, bandNamestr);
                bandcheck.setString(2, countid);
                if (!bandYearstr.equals("")) {
                    bandcheck.setString(3, bandYearstr);
                    bandcheck.setString(4, bandYearstr);
                } else {
                    bandcheck.setNull(3, Types.OTHER);
                    bandcheck.setNull(4, Types.OTHER);
                }
                if (!bandFinYearstr.equals("")) {
                    bandcheck.setString(5, bandFinYearstr);
                    bandcheck.setString(6, bandFinYearstr);
                } else {
                    bandcheck.setNull(5, Types.OTHER);
                    bandcheck.setNull(6, Types.OTHER);
                }
                resultSet = bandcheck.executeQuery();
                if (!resultSet.isBeforeFirst()) {
                    bandst = connection.prepareStatement("REPLACE INTO band (name, country_id," +
                            " formation_year, final_year) VALUES (?, ?, ?, ?);");
                    bandst.setString(1, bandNamestr);
                    bandst.setString(2, countid);
                    if (!bandYearstr.equals("")) {
                        bandst.setString(3, bandYearstr);
                    } else {
                        bandst.setNull(3, Types.OTHER);
                    }
                    if (!bandFinYearstr.equals("")) {
                        bandst.setString(4, bandFinYearstr);
                    } else {
                        bandst.setNull(4, Types.OTHER);
                    }
                    bandst.executeUpdate();
                }
                bandcheck.close();
            }
            //track_genre
            String genid = "";
            String trid = "";
            trackgenre = connection1.prepareStatement(
                    "SELECT track_id FROM track" + " WHERE LOWER (name)=? AND length=? AND " +
                            "(language_id=? OR(language_id IS NULL AND ? IS NULL)) AND release_year=?;");
            trackgenre.setString(1, trackNamestr);
            trackgenre.setString(2, trackLengthstr);
            if (languageid.equals("")) {
                trackgenre.setNull(3, Types.TINYINT);
                trackgenre.setNull(4, Types.TINYINT);
            } else {
                trackgenre.setString(3, languageid);
                trackgenre.setString(4, languageid);
            }
            trackgenre.setString(5, trackReleaseYearstr);
            resultSet = trackgenre.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                trid = resultSet.getString("track_id");
            } else {
                PreparedStatement trackgenre14 = connection1.prepareStatement("SELECT max(track_id)+1 " +
                        "FROM track;");
                resultSet = trackgenre14.executeQuery();
                resultSet.next();
                trid = resultSet.getString("max(track_id)+1");
                trackgenre14.close();
            }
            PreparedStatement trackgenre1 = connection1.prepareStatement("SELECT genre_id FROM genre WHERE LOWER(name)=?;");
            trackgenre1.setString(1, genreNamestr);
            resultSet = trackgenre1.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                genid = resultSet.getString("genre_id");
            } else {
                PreparedStatement trackgenre11 = connection1.prepareStatement("SELECT max(genre_id)+1 " +
                        "FROM genre;");
                resultSet = trackgenre11.executeQuery();
                resultSet.next();
                genid = resultSet.getString("max(genre_id)+1");
                trackgenre11.close();
            }
            trackgenre2 = connection.prepareStatement("REPLACE INTO track_genre (track_id, genre_id) VALUES " +
                    "(?,?);");
            trackgenre2.setString(1, trid);
            trackgenre2.setString(2, genid);
            trackgenre2.executeUpdate();
            trackgenre1.close();
            trackgenre.close();
            //band_artist
            String bandid = "";
            String artid = "";
            PreparedStatement bandartistcheck = connection1.prepareStatement("SELECT band_id FROM band WHERE" +
                    " LOWER (name)=? AND (country_id=? OR(country_id IS NULL AND ? IS NULL)) " +
                    "AND (formation_year=? OR(formation_year IS NULL AND ? IS NULL)) AND (final_year=? OR(final_year IS NULL AND ? IS NULL));");
            bandartistcheck.setString(1, bandNamestr);
            if (countryNamestr.equals("")) {
                bandartistcheck.setNull(2, Types.TINYINT);
                bandartistcheck.setNull(3, Types.TINYINT);
            } else {
                bandartistcheck.setString(2, countid);
                bandartistcheck.setString(3, countid);
            }
            if (bandYearstr.equals("")) {
                bandartistcheck.setNull(5, Types.OTHER);
                bandartistcheck.setNull(4, Types.OTHER);
            } else {
                bandartistcheck.setString(5, bandYearstr);
                bandartistcheck.setString(4, bandYearstr);
            }
            if (bandFinYearstr.equals("")) {
                bandartistcheck.setNull(7, Types.OTHER);
                bandartistcheck.setNull(6, Types.OTHER);
            } else {
                bandartistcheck.setString(7, bandFinYearstr);
                bandartistcheck.setString(6, bandFinYearstr);
            }
            resultSet = bandartistcheck.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                bandid = resultSet.getString("band_id");
            } else {
                PreparedStatement bandidelse = connection1.prepareStatement("SELECT max(band_id)+1 " +
                        "FROM band;");
                resultSet = bandidelse.executeQuery();
                resultSet.next();
                bandid = resultSet.getString("max(band_id)+1");
                bandidelse.close();
            }
            bandartist = connection1.prepareStatement("SELECT artist_id FROM artist" +
                    " WHERE LOWER (first_name)=? AND LOWER (last_name)=? AND " +
                    "(LOWER (alias)=? OR(alias IS NULL AND ? IS NULL))" +
                    "AND (country_id=? OR(country_id IS NULL AND ? IS NULL)) AND " +
                    "(date_of_birth=? OR(date_of_birth IS NULL AND ? IS NULL)) AND" +
                    "(date_of_death=? OR(date_of_death IS NULL AND ? IS NULL));");
            bandartist.setString(1, artistFirstNamestr);
            bandartist.setString(2, artistLastNamestr);
            if (artistAliasstr.equals("")) {
                bandartist.setNull(3, Types.VARCHAR);
                bandartist.setNull(4, Types.VARCHAR);
            } else {
                bandartist.setString(3, artistAliasstr);
                bandartist.setString(4, artistAliasstr);
            }
            if (!countid.equals("")) {
                bandartist.setString(5, countid);
                bandartist.setString(6, countid);
            } else {
                bandartist.setNull(5, Types.TINYINT);
                bandartist.setNull(6, Types.TINYINT);
            }
            LocalDate date = artistDateDeath.getValue();
            if (date != null) {
                Date date2 = Date.valueOf(date);
                bandartist.setDate(7, date2);
                bandartist.setDate(8, date2);
            } else {
                bandartist.setNull(7, Types.DATE);
                bandartist.setNull(8, Types.DATE);
            }
            date = artistDateBirth.getValue();
            if (date != null) {
                Date date2 = Date.valueOf(date);
                bandartist.setDate(9, date2);
                bandartist.setDate(10, date2);
            } else {
                bandartist.setNull(9, Types.DATE);
                bandartist.setNull(10, Types.DATE);
            }
            resultSet = bandartist.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                artid = resultSet.getString("artist_id");
            } else {
                PreparedStatement artidelse = connection1.prepareStatement("SELECT max(artist_id)+1 " +
                        "FROM artist;");
                resultSet = artidelse.executeQuery();
                resultSet.next();
                artid = resultSet.getString("max(artist_id)+1");
                artidelse.close();
            }
            bandartist1 = connection.prepareStatement("REPLACE INTO band_artist(band_id, artist_id," +
                    " start_year, end_year) VALUES (?,?,?,?);");
            bandartist1.setString(1, bandid);
            bandartist1.setString(2, artid);
            bandartist1.setString(3, bandartistStartYearstr);
            if (bandartistEndYearstr.equals(""))
                bandartist1.setNull(4, Types.OTHER);
            else
                bandartist1.setString(4, bandartistEndYearstr);
            bandartist1.executeUpdate();
            bandartist.close();
            bandartistcheck.close();
            //band_genre
            bandgenre = connection.prepareStatement("REPLACE INTO band_genre(band_id, genre_id) VALUES(?,?);");
            bandgenre.setString(1, bandid);
            bandgenre.setString(2, genid);
            bandgenre.executeUpdate();
            //album
            String lblid = "";
            if (!lableNamestr.equals("")) {
                PreparedStatement albumchecker = connection1.prepareStatement("SELECT label_id FROM label WHERE " +
                        "LOWER(name)=?;");
                albumchecker.setString(1, lableNamestr);
                resultSet = albumchecker.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    resultSet.next();
                    lblid = resultSet.getString("label_id");
                } else {
                    PreparedStatement albumcheck6 = connection1.prepareStatement("SELECT max(label_id)+1 " +
                            "FROM label ;");
                    resultSet = albumcheck6.executeQuery();
                    resultSet.next();
                    lblid = resultSet.getString("max(label_id)+1");
                    albumcheck6.close();
                }
                albumchecker.close();
            }
            albumst = connection1.prepareStatement("SELECT album_id FROM album WHERE " +
                    "LOWER(name)=? AND band_id=? AND release_year=? AND length=? " +
                    "AND (label_id=? OR(label_id IS NULL AND ? IS NULL ));");
            albumst.setString(1, albumNamestr);
            albumst.setString(2, bandid);
            albumst.setString(3, albumReleaseYearstr);
            albumst.setString(4, albumLengthstr);
            if (!lblid.equals("")) {
                albumst.setString(5, lblid);
                albumst.setString(6, lblid);
            } else {
                albumst.setNull(5, Types.SMALLINT);
                albumst.setNull(6, Types.SMALLINT);
            }
            resultSet = albumst.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                album1 = connection.prepareStatement("REPLACE INTO album (name, band_id, release_year, length, label_id)" +
                        " VALUES (?,?,?,?,?);");
                album1.setString(1, albumNamestr);
                album1.setString(2, bandid);
                album1.setString(3, albumReleaseYearstr);
                album1.setString(4, albumLengthstr);
                if (!lblid.equals("")) {
                    album1.setString(5, lblid);
                } else
                    album1.setNull(5, Types.SMALLINT);
                album1.executeUpdate();
            }
            albumst.close();
            //album_genre
            String albid = "";
            PreparedStatement albumgenre1 = connection1.prepareStatement("SELECT album_id FROM album WHERE " +
                    "LOWER(name)=? AND band_id=? AND release_year=? AND length=? AND label_id=?;");
            albumgenre1.setString(1, albumNamestr);
            albumgenre1.setString(2, bandid);
            albumgenre1.setString(3, albumReleaseYearstr);
            albumgenre1.setString(4, albumLengthstr);
            albumgenre1.setString(5, lblid);
            resultSet = albumgenre1.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                albid = resultSet.getString("album_id");
            } else {
                PreparedStatement albumgenre21 = connection1.prepareStatement("SELECT max(album_id)+1 " +
                        "FROM album;");
                resultSet = albumgenre21.executeQuery();
                resultSet.next();
                albid = resultSet.getString("max(album_id)+1");
                albumgenre21.close();
            }
            albumgenre = connection.prepareStatement("REPLACE INTO album_genre(album_id, genre_id) " +
                    "VALUES (?,?);");
            albumgenre.setString(1, albid);
            albumgenre.setString(2, genid);
            albumgenre.executeUpdate();
            //album_track
            albumTrack = connection.prepareStatement("REPLACE INTO album_track(album_id, track_id, track_number) " +
                    "VALUES (?,?,?); ");
            albumTrack.setString(1, albid);
            albumTrack.setString(2, trid);
            albumTrack.setString(3, albumtrackTrackNumberstr);
            albumTrack.executeUpdate();
            resultSet.close();
            connection.commit();
            statusline.clear();
            statusline.appendText("Script done");
        } catch (Exception e) {
            statusline.clear();
            statusline.appendText(e.getMessage());
            if (connection != null) {
                try {
                    statusline.appendText("\nTransaction is being rolled back");
                    connection.rollback();
                } catch (Exception excep) {
                    statusline.appendText((excep.getMessage()));
                }
            }
        } finally {
            if (genrest != null) {
                genrest.close();
            }
            if (labelst != null) {
                labelst.close();
            }
            if (countryst != null) {
                countryst.close();
            }
            if (track1 != null) {
                track1.close();
            }
            if (trackst != null) {
                trackst.close();
            }
            if (artistst != null) {
                artistst.close();
            }
            if (bandst != null) {
                bandst.close();
            }
            if (trackgenre2 != null) {
                trackgenre2.close();
            }
            if (bandartist1 != null) {
                bandartist1.close();
            }
            if (bandgenre != null) {
                bandgenre.close();
            }
            if (album1 != null) {
                album1.close();
            }
            if (albumgenre != null) {
                albumgenre.close();
            }
            if (albumTrack != null) {
                albumTrack.close();
            }
            connection.setAutoCommit(true);
            connection.close();
            connection1.close();
        }
    }

    @FXML
    private void initialize() throws Exception {
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) exit.getScene().getWindow();
                stage.close();
            }
        });
        mainmenu.setOnAction((event) -> {
            try {
                back(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        });
        addData.setOnAction((event -> {
            try {
                addInterf(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        }));
        clear.setOnAction((event -> {
            try {
                clearbt(event);
            } catch (Exception e) {
                statusline.clear();
                statusline.appendText(e.getMessage());
            }
        }));
    }
}

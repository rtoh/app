package com.devbase.mavejavafxapp;

;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MainApp extends Application {

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    //программа начинает работать с этого места(запкскает гуи)
    public void start(Stage stage) throws Exception {
        String fxmlFile = "/fxml/DBappJavaFX.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));
        stage.setTitle("DBapp");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
}

CREATE TABLE genre
(
  genre_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(127) NOT NULL,
  PRIMARY KEY (genre_id)
);

CREATE TABLE label
(
  label_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (label_id)
);

CREATE TABLE country
(
  country_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(127) NOT NULL,
  PRIMARY KEY (country_id)
);

CREATE TABLE language
(
  language_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(127) NOT NULL,
  PRIMARY KEY (language_id)
);

CREATE TABLE track
(
  track_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  length SMALLINT UNSIGNED NOT NULL,
  language_id TINYINT UNSIGNED DEFAULT NULL,
  text TEXT DEFAULT NULL,
  release_year YEAR NOT NULL,
  PRIMARY KEY (track_id),
  CONSTRAINT `fk_track_language` FOREIGN KEY (language_id) REFERENCES language(language_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

ALTER TABLE track CHANGE COLUMN text text TEXT COLLATE utf8_general_ci

CREATE TABLE artist
(
  artist_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(127) NOT NULL,
  last_name VARCHAR(127) NOT NULL,
  alias VARCHAR(255) DEFAULT NULL,
  country_id TINYINT UNSIGNED DEFAULT NULL,
  date_of_birth DATE DEFAULT NULL,
  date_of_death DATE DEFAULT NULL,
  PRIMARY KEY (artist_id),
  CONSTRAINT `fk_artist_country` FOREIGN KEY (country_id) REFERENCES country(country_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE band
(
  band_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  country_id TINYINT UNSIGNED DEFAULT NULL,
  formation_year YEAR DEFAULT NULL,
  final_year YEAR DEFAULT NULL,
  PRIMARY KEY (band_id),
  CONSTRAINT `fk_band_country` FOREIGN KEY (country_id) REFERENCES country(country_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE track_genre
(
  track_id INT UNSIGNED NOT NULL,
  genre_id SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (track_id, genre_id),
  CONSTRAINT `fk_track_genre_track` FOREIGN KEY (track_id) REFERENCES track(track_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_track_genre_genre` FOREIGN KEY (genre_id) REFERENCES genre(genre_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE band_artist
(
  band_id INT UNSIGNED NOT NULL,
  artist_id INT UNSIGNED NOT NULL,
  start_year YEAR NOT NULL,
  end_year YEAR DEFAULT NULL,
  PRIMARY KEY (artist_id, band_id),
  CONSTRAINT `fk_band_artist_artist` FOREIGN KEY (artist_id) REFERENCES artist(artist_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_band_artist_band` FOREIGN KEY (band_id) REFERENCES band(band_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE band_genre
(
  band_id INT UNSIGNED NOT NULL,
  genre_id SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (band_id, genre_id),
  CONSTRAINT `fk_band_genre_band` FOREIGN KEY (band_id) REFERENCES band(band_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_band_genre_genre` FOREIGN KEY (genre_id) REFERENCES genre(genre_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE album
(
  album_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  band_id INT UNSIGNED NOT NULL,
  release_year YEAR NOT NULL,
  length SMALLINT UNSIGNED NOT NULL,
  label_id SMALLINT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (album_id),
  CONSTRAINT `fk_album_label` FOREIGN KEY (label_id) REFERENCES label(label_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_album_band` FOREIGN KEY (band_id) REFERENCES band(band_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE album_genre
(
  album_id INT UNSIGNED NOT NULL,
  genre_id SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (album_id, genre_id),
  CONSTRAINT `fk_album_genre_genre` FOREIGN KEY (genre_id) REFERENCES genre(genre_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_album_genre_album` FOREIGN KEY (album_id) REFERENCES album(album_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE album_track
(
  album_id INT UNSIGNED NOT NULL,
  track_id INT UNSIGNED NOT NULL,
  track_number TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (track_id, album_id),
  CONSTRAINT `fk_album_track_track` FOREIGN KEY (track_id) REFERENCES track(track_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_album_track_album` FOREIGN KEY (album_id) REFERENCES album(album_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
